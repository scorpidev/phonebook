extern crate postgres;
extern crate ini;

use postgres::{Client, Error, NoTls};
use std::unimplemented;
use ini::Ini;

struct ConnectParams {
    host: String,
    user: String,
    password: String,
}

impl ConnectParams {
    fn as_string(&self) -> String {
        format!("host={} user={} password={}", self.host, self.user, self.password)
    }

    fn new(host: String, user: String, password: String) -> ConnectParams {
        ConnectParams {
            host,
            user, 
            password
        }
    }
}

fn prepare_params() -> (ConnectParams, NoTls) {
    let conf = Ini::load_from_file(".phonebookrc").unwrap();
    let general = conf.general_section();

    let host = general.get("host").unwrap().to_string();
    let user = general.get("user").unwrap().to_string();
    let password = general.get("password").unwrap().to_string();
    let tls_mode = general.get("tls_mode").unwrap();

    let tls_mode = match tls_mode.as_ref() {
        "disable" => NoTls,
        "enable" => unimplemented!(),
        _ => panic!("Wrong sslmode")
    };

    let params = ConnectParams::new(
        host,
        user,
        password,
    );
    
    (params, tls_mode)
}

pub fn get_connection_client() -> Client {
    let (conn_params, tls_mode) = prepare_params();
    match Client::connect(&conn_params.as_string()[..], tls_mode) {
        Ok(client) => return client,
        Err(e) => {
            println!("Ошибка при подключении к базе данных:\n{}", e);
            std::process::exit(0)
        }
    }
}

pub fn create_main_table(cli: &mut Client) {
    match cli.batch_execute("
        CREATE TABLE IF NOT EXISTS phonebook (
            id      SERIAL PRIMARY KEY,
            name    VARCHAR(50) NOT NULL,
            phone   VARCHAR(100) NOT NULL
        )"
    ) {
        Ok(()) => {
            println!("Таблица успешно создана или уже существует.")
        },
        Err(e) => {
            println!("Произошла ошибка при создании основной таблицы.\n{}", e);
            std::process::exit(0)
        }
    }
}

pub fn insert(mut db: Client, name: &str, phone: &str) -> Result<u64, Error> {
    db.execute(
        "INSERT INTO phonebook (name, phone) VALUES ($1, $2)",
        &[&name, &phone]
    )
}

pub fn remove(mut db: Client, ids: &[i32]) -> Result<u64, Error> {
    let stmt = db.prepare("DELETE FROM phonebook WHERE id=$1").unwrap();
    for id in ids {
        db.execute(&stmt, &[id])?;
    }
    Ok(0)
}

pub fn update(mut cli: Client, id: i32, name: &str, phone: &str) -> Result<(), Error> {
    let mut transaction = cli.transaction()?;
    transaction.execute("UPDATE phonebook SET name = $1, phone = $2 WHERE id = $3",
    &[&name, &phone, &id]).unwrap();
    transaction.commit()
}

pub fn show(mut cli: Client, arg: Option<&str>) -> Result<Vec<Record>, Error> {
    let condition_str = match arg {
        Some(s) => format!("WHERE name LIKE '%{}%'", s),
        None => "".to_owned()
    };
    let statement = cli.prepare(
        &format!("SELECT * FROM phonebook {} ORDER BY id", condition_str)
    ).unwrap();
    let rows = cli.query(&statement,&[]).unwrap();
    let size = rows.len();
    let mut result = Vec::with_capacity(size);
    for row in rows {
        let record = Record {
            id: row.get("id"),
            name: row.get("name"),
            phone: row.get("phone"),
        };
        result.push(record)
    }
    Ok(result)
}

pub struct Record {
    id: i32,
    name: String,
    phone: String,
}

impl Record {
    pub fn new(id: i32, name: String, phone: String) -> Record {
        Record {
            id,
            name,
            phone,
        }
    }

    pub fn as_str(&self) -> String {
        format!("{}, {}, {}", self.id, self.name, self.phone)
    }

    pub fn as_json_str(&self) -> String {
        format!(r#"{{"id":{}, "name":"{}", "phone":"{}"}}"#, self.id, self.name, self.phone)
    }
}

pub fn format(records: &[Record]) {
    let max = records.iter().fold(
        0, 
        |acc, ref item|
        if item.name.len() > acc { item.name.len() } else { acc });
    for record in records {
        println!("{:3}  {:.*}   {}", record.id, max, record.name, record.phone)
    }
}