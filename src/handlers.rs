extern crate iron;
extern crate postgres;
extern crate params;

use crate::db;

use iron::prelude::*;

use params::{Params, Value};
use postgres::Client;
use std::sync::{Mutex, Arc};
use iron::{IronResult, Request, Response, status};


pub fn get_records(scli: Arc<Mutex<Client>>, _req: &mut Request) -> IronResult<Response> {
    let mut scli = scli.lock().unwrap();
    let rows = scli.query("SELECT * FROM phonebook ORDER BY name", &[]).unwrap();
    let mut response = Response::new();
    response.headers.append_raw("content-type", b"application/json; charset=utf-8".to_vec());
    let mut response_body = String::from("[");
    let records_json = rows.iter().map(|row| {
        db::Record::new(
            row.get("id"),
            row.get("name"),
            row.get("phone")
        ).as_json_str()
    }).collect::<Vec<String>>().join(", ");
    response_body.push_str(&records_json);
    response_body.push_str("]");
    response.status = Some(status::Ok);
    response.body = Some(Box::new(response_body));
    Ok(response)
}

pub fn get_record(scli: Arc<Mutex<Client>>, req: &mut Request) -> IronResult<Response> {
    let mut scli = scli.lock().unwrap();
    let record_id: i32 = req.url.path()[3].parse().unwrap();
    let mut response = Response::new();
    response.headers.append_raw("content-type", b"application/json; charset=utf-8".to_vec());
    match scli.query_one(
        "SELECT * FROM phonebook WHERE id=$1",
        &[&record_id]
    ) {
        Ok(row) => {
            let record = db::Record::new(
                row.get("id"),
                row.get("name"),
                row.get("phone")
            );
            let response_body = format!("{}\n", record.as_json_str());
            response.status = Some(status::Ok);
            response.body = Some(Box::new(response_body));
        },
        Err(e) => {
            response.status = Some(status::BadRequest);
            response.body = Some(Box::new(format!("<p>Запрос к базе данных завершился ошибкой:</p><p><b>{}</b></p>", e)));
        }
    };
    Ok(response)
}

pub fn edit_record(scli: Arc<Mutex<Client>>, req: &mut Request) -> IronResult<Response> {
    let mut scli = scli.lock().unwrap();
    let record_id: i32 = req.url.path()[3].parse().unwrap();
    
    let map = req.get_ref::<Params>().unwrap();
    let (name, phone) = match map.find(&["name"]) {
        Some(&Value::String(ref name)) => {
            match map.find(&["phone"]) {
                Some(&Value::String(ref phone)) => {
                    let mut transaction = scli.transaction().unwrap();
                    transaction.execute(
                        "UPDATE phonebook SET name = $1, phone = $2 WHERE id = $3",
                        &[&name, &phone, &record_id]
                    ).unwrap();
                    match transaction.commit() {
                        Ok(()) => {},
                        Err(e) => {
                            std::mem::drop(scli);
                            panic!("Error with database transaction: {}", e)
                        }
                    }
                    (name, phone)
                },
                _ => {
                    std::mem::drop(scli);
                    panic!("Parameter 'phone' must be getted")
                }
            }
        },
        _ => { 
            std::mem::drop(scli);
            panic!("Parameter 'name' must be getted")
        }
    };

    let response_body = format!("Запись '{}' была изменена с параметрами: {} {}", record_id, name, phone);
    Ok(Response::with((status::Ok, response_body )))
}

pub fn delete_record(scli: Arc<Mutex<Client>>, req: &mut Request) -> IronResult<Response> {
    let mut scli = scli.lock().unwrap();
    let record_id: i32 = req.url.path()[3].parse().unwrap();
    match scli.execute("DELETE FROM phonebook WHERE id=$1",&[&record_id]) {
        Ok(_n) => {
            Ok(Response::with((status::Ok, format!("Запись '{}' удалена", record_id))))
        },
        Err(e) => {
            Ok(Response::with((status::Ok, format!("Возникла ошибка при удалении записи:\n{}", e))))
        }
    }
}


