extern crate postgres;
extern crate ini;
extern crate iron;
extern crate router;

use core::panic;
use std::sync::{Arc, Mutex};
use postgres::Client;
use iron::prelude::*;
use iron::Iron;
use router::Router;

mod db;
mod handlers;

const HELP: &'static str = "Usage: phonebook COMMAND [ARG]...
Commands:
    add NAME PHONE - create new record;
    del ID1 ID2... - delete record;
    edit ID        - edit record;
    show           - display all records;
    show STRING    - display records which contain a given substring in the name;
    help           - display this help.
    \n";

fn start_server(cli: Client) {
    println!("Запуск сервера...");
    let scli = Arc::new(Mutex::new(cli));
    let mut router = Router::new();
    {
        let scli_ = scli.clone();
        router.get(
            "/api/v1/records",
            move |req: &mut Request| handlers::get_records(scli_.clone(), req),
            "records"
        );
    }
    {
        let scli_ = scli.clone();
        router.get(
            "api/v1/records/:id",
            move |req: &mut Request| handlers::get_record(scli_.clone(), req),
            "record"
        );
    }
    {
        let scli_ = scli.clone();
        router.put(
            "api/v1/records/:id/edit", 
            move |req: &mut Request| handlers::edit_record(scli_.clone(), req),
            "edit_record"
        );
    }
    {
        let scli_ = scli.clone();
        router.post(
            "api/v1/records/:id/delete", 
            move |req: &mut Request| handlers::delete_record(scli_.clone(), req),
            "delete_record"
        );
    }
    match Iron::new(router).http("localhost:3000") {
        Ok(_listener) => { println!("Сервер успешно запущен:\nhttp://localhost:3000/") },
        Err(e) => { println!("Возникла ошибка при запуске сервера:\n{}", e) }
    }
}

fn main() {
    let mut cli = db::get_connection_client();

    db::create_main_table(&mut cli);
    
    let args: Vec<String> = std::env::args().collect();
    match args.get(1) {
        Some(text) => {
            match text.as_ref() {
                "add" => {
                    if args.len() != 4 {
                        panic!("Использование: phonebook add ИМЯ ТЕЛЕФОН");
                    }
                    let r = db::insert(cli, &args[2], &args[3]).unwrap();
                    println!("{} rows affected", r)
                },
                "del" => {
                    if args.len() < 3 {
                        panic!("Использование: phonebook del ID...");
                    }
                    let ids: Vec<i32> = args[2..].iter()
                        .map(|s| s.parse().unwrap())
                        .collect();
                    db::remove(cli, &ids).unwrap();
                },
                "edit" => {
                    if args.len() != 5 {
                        panic!("Использование: phonebook edit ID ИМЯ ТЕЛЕФОН");
                    }
                    let id = args[2].parse().unwrap();
                    db::update(cli, id, &args[3], &args[4]).unwrap();
                },
                "show" => {
                    if args.len() > 3 {
                        panic!("Использование: phonebook show [ЧАСТЬ ИМЕНИ]")
                    }
                    let s;
                    if args.len() == 3 {
                        s = args.get(2);
                    } else {
                        s = None;
                    }
                    let rows = db::show(cli, s.as_ref().map(|s| &s[..])).unwrap();
                    db::format(&rows);
                },
                "help" => { println!("{}", HELP) },
                "serve" => { start_server(cli) },
                command @ _ => {
                    println!("Неправильная команда: {}", command);
                    println!("{}", HELP);
                }
            }
        },
        None => {
            println!("Не было передано команды");
            println!("{}", HELP);
        },
        
    }
}
